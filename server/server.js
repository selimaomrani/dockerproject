const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

var app = express();
//middleware
app.use(bodyParser.json());
app.use(cors());

mongoose.connect("mongodb://proj_docker_mongodb_1:27017/dockerDB", err => {
  if (err) {
    console.log("error", err);
  } else {
    console.log("connected to mongodb");
  }
});

app.get("/", (req, res) => {
  res.send("hello");
});
app.listen(8080, err => {
  if (err) {
    console.log("could not start server");
  } else {
    console.log("server running on port : 8080");
  }
});
